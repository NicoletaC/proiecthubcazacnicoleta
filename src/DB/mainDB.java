package DB;

import java.sql.*;

public class mainDB {
    private static Connection connect = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static String url = "jdbc:mysql://localhost:3306/dj2111ro"; // /databaseName
    private static String user = "root", pass = "root";


    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            connect = DriverManager.getConnection(url , user, pass );

            statement = connect.createStatement();

            resultSet = statement.executeQuery("select * from studenti");

            while(resultSet.next()){
                int studentId = resultSet.getInt(1);
                String nume = resultSet.getString(2);
                String prenume = resultSet.getString(3);
                Date anulNasterii = resultSet.getDate(4);
                System.out.println(studentId +  " " + nume + " " + prenume + " " + anulNasterii);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
